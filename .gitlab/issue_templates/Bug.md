#### Software 
- Coscine :heavy_check_mark: 
- Python SDK :x:

<!--description-->
#### Description:  

Description goes here...

<!--description#-->

<!--additional-->
##### Acceptance Criteria:  
- [ ] 
- [ ] 
- [ ] 

<!--additional#-->

<!--checklists-->
---
#### Definition of Done:
* [ ] Clear description of how to test this Issue is present
<!--checklists#-->



<!--description-->
#### Description of the :bug:  

Description goes here...

<!--description#-->

<!--additional-->
##### How to Reproduce:
1. 
2. 
3. 
4. 

##### Technical Report:


##### How it Should Look Like:
Description...

<!--additional#-->

<!--checklists-->
---
#### Definition of Done:
* [ ] Clear description of how to test this Hotfix is present
* [ ] Document the necessary database changes in the changelog
* [ ] Write unit tests for new PHP classes and changes in our own classes
* [ ] Run PHP unit tests locally
<!--checklists#-->


/label ~"Bug"
